import React, { Component } from "react";
import "../styles/App.css";
import NewsGrid from "../components/NewsGrid";
import NavBar from "../components/NavBar";

const newsApi =
  "https://newsapi.org/v2/everything?q=*&sortBy=relevance&pageSize=100&language=en&apiKey=09564f092fe34a70989b565ac77b8e29";
const proxy = "https://cors.bridged.cc/";
class App extends Component {
  constructor() {
    super();
    this.state = {
      news: [],
    };
  }

  componentDidMount() {
    fetch(proxy + newsApi)
      .then((response) => response.json())
      .then((articles) => {
        this.setState({ news: articles.articles });
      });
  }

  render() {
    const { news } = this.state;
    return !news.length ? (
      <h1 className="tc fw3">Loading...</h1>
    ) : (
      <div className="tc">
        <NavBar />
        <section>
          <NewsGrid news={news} />
        </section>
      </div>
    );
  }
}

export default App;
