import React from 'react';
import '../styles/NewsCard.css';

const NewsCard = ({ image, title, description, date, url }) => (
  <div className='card-container grow'>
    <a className='card-anchor' href={url}>
      <article className='br2 ba dark-gray b--black-10 mv1 w-90 w-90-m w-90-l mw6-ns mw5-l center card-element shadow-5'>
        <img src={image} className='db w-100 br2 br--top' alt=' ' />
        <div className='pa2 ph3-ns pb3-ns'>
          <div className='dt w-100 mt1'>
            <div className='dtc'>
              <h1 className='card-title mv0'>{title}</h1>
            </div>
          </div>
          <p className='lh-copy measure mt2 mid-gray card-description'>
            {description}
          </p>
          <div className='dtc tr'>
            <h2 className='card-date mv0'>{date}</h2>
          </div>
        </div>
      </article>
    </a>
  </div>
);

export default NewsCard;
