import React from 'react';
import NewsCard from './NewsCard';

const NewsGrid = ({ news }) => (
  <div className=''>
    {news.map((article, i) => {
      return (
        <NewsCard
          key={i}
          image={news[i].urlToImage}
          title={news[i].title}
          description={news[i].description}
          date={news[i].publishedAt}
          url={news[i].url}
        />
      );
    })}
  </div>
);

export default NewsGrid;
