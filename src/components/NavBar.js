import React from 'react';
import '../styles/NavBar.css';

const NavBar = () => (
  <div classame='nv-container'>
    <header className='bg-white-90 w-100 title shadow-2'>
      <div className='f2 pb2 pt2' style={{ display: 'flex' }}>
        <div>React</div> <div className='blue'>News</div>
      </div>
    </header>
  </div>
);

export default NavBar;
